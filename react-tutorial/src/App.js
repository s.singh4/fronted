import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./components/Login/Login";
import { Home } from "./components/Home/Home";
import { useLog } from "./GlobalState";



function App() {
  
  return (
    
      
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/home" element={<Home />} />
      </Routes>
    </BrowserRouter>
    
    
  );
}
export default App;
