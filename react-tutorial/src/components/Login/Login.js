import React, { useState, useEffect } from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useNavigate } from "react-router-dom";
import { useLog, useSetLog } from "../../GlobalState";
import { LoggedContext } from "../../GlobalState";

function Login() {
  const pStyle = { padding: 20, height: 450, width: 450, margin: "50px auto" };
  const bStyle = { padding: 20 };
  const navigate = useNavigate();

  const [isloggedIn, setisloggedIn] = useState(false);
  useEffect(() => {
    localStorage.setItem("sign", JSON.stringify(isloggedIn));
  }, [isloggedIn]);

  useEffect(() => {
    const newval = JSON.parse(localStorage.getItem("sign"));
    if (newval == true) {
      navigate("/home");
    }
  });

  const handleclick = (e) => {
    if (val.user == val.pass && val.pass == "admin") {
      setisloggedIn(true);
    }
  };

  const [val, setval] = useState({ user: "", pass: "" });

  return (
    <Grid>
      <Paper className="paper" elevation={3} style={pStyle}>
        <Grid align="center">
          <h1>SignIn</h1>
          <br></br>
        </Grid>
        <TextField
          id="outlined-basic"
          label="UserName"
          variant="outlined"
          placeholder="Enter UserName"
          fullWidth={true}
          value={val.user}
          onChange={(e) => setval({ user: e.target.value, pass: val.pass })}
        />
        <br></br>
        <br></br>
        <TextField
          type="password"
          id="outlined-basic"
          label="Password"
          variant="outlined"
          placeholder="Enter Password"
          fullWidth={true}
          value={val.pass}
          onChange={(e) => setval({ user: val.user, pass: e.target.value })}
        />
        <br></br>
        <br></br>
        <Button
          type="submit"
          onClick={handleclick}
          variant="contained"
          fullWidth="true"
          style={bStyle}
        >
          Sign In
        </Button>
      </Paper>
    </Grid>
  );
}

export default Login;
