import React from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";



export function Home() {
  const pStyle = { padding: 20, height: 450, width: 450, margin: "50px auto" };
  return (
    <Grid>
      <Paper elevation={2} style={pStyle}>
        <h1>Hi..! Welcome to Home Page</h1>
      </Paper>
    </Grid>
  );
}


