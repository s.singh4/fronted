import React, { createContext, useContext, useState } from "react";


export const LoggedContext = React.createContext();
const ToggleContext = React.createContext();

export function useLog(){
  return useContext(LoggedContext);
}

export function useSetLog(){
  return useContext(ToggleContext);
}

export function Refrence({ children }) {
  const [isLoggedIn, setisLoggedIn] = useState(false);
  function ToggleLog() {
    setisLoggedIn(previsLoggedIn => !previsLoggedIn);
  }

  return (
    <LoggedContext.Provider value={isLoggedIn}>
      <ToggleContext.Provider value={ToggleLog}> 
        {children}
      </ToggleContext.Provider>
      
    </LoggedContext.Provider>
  );
}