import React from "react";
import { useState, useEffect } from "react";
import "./App.css";

function App() {
  const [user, setUser] = useState([]);
  useEffect(() => {
    fetch('/users')
      .then((res) => res.json())
      .then((users) => {
        console.log({users})
        setUser(users)
      
      });
     
  }, []);
  return (
    <div className="App">
      
      <h1>Users</h1>
     {user.map((item)=><div key={item.id}><h2>{item.username}</h2></div>)} 
    </div>
  );
}

export default App;
