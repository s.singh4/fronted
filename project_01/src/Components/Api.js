import { client } from "./Service";
import axios from "axios";

export const fetchFunctionData = async () => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Function";

  const response = await client.post(
    `${baseURL}/search`,
    {
      fields: ["id", "name", "code", "version"],
      sortBy: null,
      data: { _domainContext: {} },
      limit: 10,
      offset: 0,
      translate: true,
    },
    {}
  );

  return response.data.data;
};
export const fetchMainCompany = async () => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Partner";

  const response = await client.post(
    `${baseURL}/search`,
    {
      fields: ["fullName"],

      sortBy: ["name"],
      data: {
        _domainContext: {
          companySet: [{ name: "Axelor", id: 1 }],
          _model: "com.axelor.apps.base.db.Partner",
        },
        _domain:
          "self.isContact = false  AND self in (SELECT p FROM Partner p join p.companySet c where c in :companySet)",
      },
      limit: 40,
      offset: 0,
      translate: true,
    },
    {}
  );

  return response.data.data;
};

export const fetchLanguage = async () => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Language";

  const response = await client.post(
    `${baseURL}/search`,
    {
      fields: ["id", "name", "code", "version"],
      sortBy: null,
      data: { _domainContext: {} },
      limit: 10,
      offset: 0,
      translate: true,
    },
    {}
  );
  return response.data.data;
};

export const fetchIDdata = async (id) => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Partner";

  if (id) {
    const response = await client.post(
      `${baseURL}/${id}/fetch`,
      {
        fields: [
          "id",
          "name",
          "fixedPhone",
          "version",
          "language",
          "jobTitleFunction",
          "firstName",
          "mainPartner",
          "picture",
          "partnerSeq",
          "user",
          "team",
        ],
      },
      {}
    );
    console.log(response);
    return response.data.data;
  }
};
export const fetchAssignedTo = async () => {
  const baseURl = "axelor-erp/ws/rest/com.axelor.auth.db.User";

  const response = await client.post(
    `${baseURl}/search`,
    {
      data: {
        _domainContext: { _model: "com.axelor.auth.db.User" },
        operator: "and",
        criteria: [],
      },
      fields: [
        "tradingName",
        "blocked",
        "name",
        "activateOn",
        "fullName",
        "expiresOn",
        "activeCompany",
        "group",
      ],
      limit: 40,
      offset: 0,
      translate: null,
    },
    {}
  );

  return response.data.data;
};
export const fetchTeams = async () => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.team.db.Team";

  const response = client.post(
    `${baseURL}/search`,
    {
      data: {
        _domainContext: { _model: "com.axelor.team.db.Team" },
        operator: "and",
        criteria: [],
      },
      fields: ["name"],

      offset: 0,

      translate: true,
    },
    {}
  );

  return response;
};
export const fetchCompany = async () => {
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Company";
  const response = client.post(
    `${baseURL}/search`,
    {
      data: { _domainContext: {} },
      fields: ["id", "name", "code"],
      translate: true,
    },
    {}
  );
  return response;
};
