import React, { createContext, useState, useCallback, useEffect } from "react";
import { client } from "./Service";

export const userContext = createContext(null);

const LIMIT = 10;

function ContextProvider({ children }) {
  const [apiData, setApiData] = useState([]);
  const [query, setQuery] = useState("");
  const [page, setPage] = useState(0);
  const [oldpage, setOldPage] = useState(0);
  const [total, setTotal] = useState(0);

  const [cardData, setcardData] = useState([]);
  const [loading, setLoading] = useState(false);
  const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Partner";
  const [open1, setOpen1] = useState(false);
  const [success1, setSuccess1] = useState();
  const [message1, setMessage1] = useState("");

  const handleSnackbarOpen1 = (success, message) => {
    setSuccess1(success);
    setOpen1(true);
    setMessage1(message);
  };

  const handleSnackbarClose1 = () => {
    setOpen1(false);
  };

  function handleDelete(id, cardData) {
    client
      .post(
        `${baseURL}/removeAll`,
        {
          records: [{ id: id }],
          data: { _domainContext: {} },
          translate: true,
        },

        {}
      )
      .then((res) => {
        if (res.data.status === 0) {
          setcardData((info) => info.filter((v) => v.id !== id));
          handleSnackbarOpen1(true, "Record Deleted Successfully..!");
        } else {
          handleSnackbarOpen1(
            false,
            "This record cannot be deleted due to refrence error"
          );
        }
      });
  }
  const fetchRecords = useCallback(() => {
    setLoading(true);
    client
      .post(
        `${baseURL}/search`,
        {
          fields: [
            "id",
            "emailAddress",
            "name",
            "fullName",
            "fixedPhone",
            "mobilePhone",
            "version",
            "simpleFullName",
            "createdOn",
            "language",
            "picture",
            "partnerSeq",
            "jobTitleFunction",
            "mainPartner.simpleFullName",
            "mainAddress",
          ],
          limit: LIMIT,
          offset: page * LIMIT,

          data: {
            _domainContext: {},
            domain:
              "self.isContact = true AND (self.isCustomer = true OR self.isProspect = true)",

            _searchText: query,
            criteria: [
              {
                fieldName: "fullName",
                operator: "like",
                value: query,
              },
            ],
          },
        },
        {}
      )
      .then((res) => {
        setcardData(res.data.data ?? []);
        setTotal(res.data.total ?? 0);
      })
      .finally(() => {
        setLoading(false);
      });
  }, [page, query]);
  useEffect(() => {
    fetchRecords();
  }, [fetchRecords]);

  return (
    <userContext.Provider
      value={{
        apiData,
        setApiData,
        query,
        setQuery,
        page,
        setPage,
        handleDelete,
        cardData,
        setcardData,
        total,
        setTotal,
        loading,
        oldpage,
        setOldPage,
        open1,
        success1,
        message1,
        handleSnackbarClose1,
      }}
    >
      {children}
    </userContext.Provider>
  );
}
export default ContextProvider;
