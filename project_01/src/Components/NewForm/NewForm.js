import React, { useCallback } from "react";
import { useState, useEffect, useRef } from "react";
import { userContext } from "../MyContext";
import ReactDraft from "../ReactDraft/ReactDraft";
import MyEditor from "../Test.js/Test";
import { useNavigate, useParams } from "react-router-dom";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import SaveIcon from "@mui/icons-material/Save";

import { useContext } from "react";
import { Button } from "@mui/material";
import Selection from "../ Selection";
import { client } from "../Service";

import "./NewForm.css";
import {
  Card,
  InputLabel,
  Paper,
  Box,
  Select,
  MenuItem,
  Input,
} from "@mui/material";

import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";

import GoogleIcon from "@mui/icons-material/Google";
import FacebookIcon from "@mui/icons-material/Facebook";
import YouTubeIcon from "@mui/icons-material/YouTube";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import TwitterIcon from "@mui/icons-material/Twitter";

import UploadIcon from "@mui/icons-material/Upload";
import CloseIcon from "@mui/icons-material/Close";

import {
  fetchFunctionData,
  fetchIDdata,
  fetchLanguage,
  fetchMainCompany,
  fetchAssignedTo,
  fetchTeams,
  fetchCompany,
} from "../Api";

const initialState = {
  name: "",
  titlesSelect: "",
  isContact: true,
  isCustomer: false,
  companySet: [{ name: "Axelor", id: 1 }],
  fixedPhone: "",
  firstName: "",

  language: null,
  jobTitleFunction: null,
  mainPartner: null,
  picture: null,
  user: null,
  team: null,
  company: null,
};

function NewForm() {
  const navigate = useNavigate();
  const { setQuery, setPage, query } = useContext(userContext);
  const [FunctionData, setFunctionData] = useState([]);
  const [LangData, setLangData] = useState([]);
  const [MainCompany, setMainCompany] = useState([]);
  const [assigned, setAssigned] = useState([]);
  const [teams, setTeams] = useState([]);
  const [company, setCompany] = useState([]);
  const [data, setData] = useState(initialState);
  const [imageL, setimageL] = useState();
  const [value, setValue] = useState("0");
  const [phoneText, setPhoneText] = useState("");
  const [hovered, setHovered] = useState(false);

  const [open, setOpen] = useState(false);
  const [success, setSuccess] = useState();
  const [message, setMessage] = useState("");

  function handleMouseEnter() {
    setHovered(true);
  }
  function handleMouseLeave() {
    setHovered(false);
  }

  const handleTabChange = (event, newValue) => {
    event.preventDefault();
    setValue(() => newValue);
    console.log(value);
  };

  const { id } = useParams();

  const handleFunction = useCallback(async () => {
    const res = await fetchFunctionData();

    console.log("????", res);
    res.push({ name: "" });
    setFunctionData(() => res);
  }, []);

  const handleMainCompany = useCallback(async () => {
    const res = await fetchMainCompany();
    res.push({ fullName: "" });
    setMainCompany(() => res);
  }, []);

  const handleLanguage = useCallback(async () => {
    const res = await fetchLanguage();
    res.push({ name: "" });
    setLangData(() => res);
  }, []);

  const handleAssignedTo = useCallback(async () => {
    const res = await fetchAssignedTo();
    res.push({ name: "" });
    setAssigned(() => res);
  }, []);

  const fetchTeamsData = useCallback(async () => {
    const res = await fetchTeams();
    res.data.data.push({ name: "" });
    setTeams(() => res.data.data);
  }, []);

  const fetchCompanyData = useCallback(async () => {
    const res = await fetchCompany();
    res.data.data.push({ name: "" });
    setCompany(() => res.data.data);
  }, []);

  function handleImage(e) {
    const file = e.target.files[0];
    console.log(file);
    const reader = new FileReader();

    reader.onload = function (e) {
      setimageL(() => e.target.result);
    };

    if (file) {
      console.log("filesssss", file);
      reader.readAsDataURL(file);
    }

    const formData = new FormData();
    formData.append("file", file);
    formData.append("field", undefined);
    formData.append(
      "request",
      JSON.stringify({
        data: {
          fileName: file.name,
          fileType: file.type,
          fileSize: file.size,
          $upload: { file: {} },
        },
      })
    );

    client
      .post(
        `axelor-erp/ws/rest/com.axelor.meta.db.MetaFile/upload`,
        formData,
        {
          headers: {
            "Content-Type": `multipart/form-data;  boundary="another cool boundary";`,

            Accept: "*/*",
          },
        },
        {}
      )
      .then((res) => {
        // console.log("......", res.data.data[0]);
        setData((prevdata) => ({
          ...prevdata,

          picture: res.data.data[0],
        }));
      });
  }
  const handleSnackbarOpen = (success, message) => {
    setSuccess(success);
    setOpen(true);
    setMessage(message);
  };

  const handleSnackbarClose = () => {
    setOpen(false);
  };

  async function handleSave(e) {
    e.preventDefault();
    console.log("after", data);

    if (
      data.name &&
      data.firstName &&
      data.jobTitleFunction &&
      data.fixedPhone &&
      data.company &&
      data.picture &&
      data.team &&
      data.user &&
      data.mainPartner &&
      data.language !== null
    ) {
      const baseURL = "axelor-erp/ws/rest/com.axelor.apps.base.db.Partner";

      client
        .post(
          `${baseURL}/`,
          {
            data: {
              ...data,
            },
          },

          {}
        )
        .then((res) => {
          setData((prevdata) => ({
            ...prevdata,
            version: res.data.data[0].version,
          }));
          if (res.status === 200) {
            console.log("hello");
            handleSnackbarOpen(true, "Record saved Successfully..!");
          }

          console.log("|||||||", res.status);

          // navigate(`/`);
          setQuery("");
          setPage(0);
          setTimeout(function () {
            navigate(`/`);
          }, 3000);
        });
    } else {
      handleSnackbarOpen(false, "Please fill up all the mandatory(*) details ");
    }
  }
  async function fetchId(id) {
    const res = await fetchIDdata(id);
    console.log("***************", res);

    setData((data) => ({
      ...data,
      name: res[0].name,
      id: res[0].id,
      fixedPhone: res[0].fixedPhone,
      fullName: res[0].fullName,
      version: res[0].version,
      firstName: res[0].firstName,
      language: { ...res[0].language },
      jobTitleFunction: { ...res[0].jobTitleFunction },
      mainPartner: { ...res[0].mainPartner },
      picture: { ...res[0].picture },
      partnerSeq: res[0].partnerSeq,
      user: { ...res[0].user },
      team: { ...res[0].team },
    }));
  }
  useEffect(() => {
    if (id) {
      fetchId(id);
    }
  }, [id]);

  useEffect(() => {
    handleFunction();
  }, [handleFunction]);

  useEffect(() => {
    fetchTeamsData();
  }, [fetchTeamsData]);

  useEffect(() => {
    fetchCompanyData();
  }, [fetchCompanyData]);

  useEffect(() => {
    handleLanguage();
  }, [handleLanguage]);

  useEffect(() => {
    handleAssignedTo();
  }, [handleAssignedTo]);

  useEffect(() => {
    handleMainCompany();
  }, [handleMainCompany]);

  function handleMainCompanyChange(value) {
    let ids = value;
    console.log("value=====", ids);
    const mainPartner = MainCompany.find((element) => element.fullName === ids);
    console.log("main", mainPartner);

    setData(() => ({ ...data, mainPartner }));
  }

  function handleFunctionChange(value) {
    let ids = value;
    const newArr = FunctionData.filter((element) => element.name === ids);
    console.log("function", newArr);
    setData(() => ({ ...data, jobTitleFunction: newArr[0] }));
  }
  function handleLanguageChange(value) {
    let ids = value;
    const newArr = LangData.filter((element) => element.name === ids);
    console.log("lang", newArr);
    setData(() => ({ ...data, language: newArr[0] }));
  }
  function assignedToChange(value) {
    let ids = value;
    const newArr = assigned.filter((element) => element.fullName === ids);
    console.log("assigned", newArr);
    setData(() => ({ ...data, user: newArr[0] }));
  }
  function teamsChange(value) {
    let ids = value;
    const newArr = teams.filter((element) => element.name === ids);
    console.log("team", newArr);
    setData(() => ({ ...data, team: newArr[0] }));
  }
  function companyChange(value) {
    let ids = value;
    const newArr = company.filter((element) => element.name === ids);

    setData(() => ({ ...data, company: newArr[0] }));
  }

  function isValidPhoneNumber(num) {
    if (num.length === 0) {
      setPhoneText(() => "Enter the Number");
    } else if (num.length < 10) {
      setPhoneText(() => "Too short");
    } else if (num.length === 10) {
      setPhoneText(() => "validate Number");
    } else {
      setPhoneText(() => "Not a Valid Number");
    }
  }

  return (
    <>
      <div className="btn_save">
        <SaveIcon onClick={handleSave} />
      </div>

      <div className="main">
        {success ? (
          <Snackbar
            open={open}
            className="snackBar"
            message={message}
            onClose={handleSnackbarClose}
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            autoHideDuration={3000}
          >
            <Alert severity="success">{message}</Alert>
          </Snackbar>
        ) : (
          <Snackbar
            open={open}
            className="snackBar"
            message={message}
            onClose={handleSnackbarClose}
            anchorOrigin={{ vertical: "top", horizontal: "right" }}
            autoHideDuration={3000}
          >
            <Alert severity="warning">{message}</Alert>
          </Snackbar>
        )}

        <Card sx={{ maxWidth: 1000 }} elevation={8} className="formcard">
          <div className="div_paper">
            <Paper variant="outlined" square className="div_inpaper"></Paper>
          </div>
          <Box className="_upper">
            <Box className="_box">
              

              <Paper
                variant="outlined"
                square
                sx={{ m: 1, width: 155, height: 160 }}
                style={{ position: "relative" }}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
              >
                {console.log("imGage", data?.picture?.id)}

                {data?.picture?.id ? (
                  <img
                    id="image_input"
                    required
                    crossOrigin=""
                    src={
                      id
                        ? `axelor-erp/ws/rest/com.axelor.meta.db.MetaFile/${data?.picture.id}/content/download?image=true&v=${data?.picture.version}&parentId=${id}&parentModel=com.axelor.meta.db.Meta`
                        : imageL
                    }
                    height="150px"
                    width="150px"
                    alt="ProfilePic *"
                  />
                ) : (
                  <img src="" height="150px" width="150px" required alt="ProfilePic *" />
                )}

                <Paper
                  className="hovered_"
                  // variant="outlined"
                  elevation={3}
                  square
                  style={{ display: !hovered ? "none" : "block" && "flex" }}
                >
                  <InputLabel className="photo_input">
                    <UploadIcon className="upload_" />
                    <input
                      className="photo_input"
                      name="profile_pic"
                      type="file"
                      id="i_input"
                      accept=".jpg, .jpeg, .png"
                      onChange={handleImage}
                    />
                  </InputLabel>
                  <CloseIcon
                    className="close_"
                    onClick={() => setData({ ...data, picture: "" })}
                  />
                </Paper>
              </Paper>

              <Box className="icon_list">
                <GoogleIcon color="primary" className="_icon" />
                <FacebookIcon className="_icon" color="primary" />
                <YouTubeIcon className="_icon" color="primary" />
                <LinkedInIcon className="_icon" color="primary" />
                <TwitterIcon className="_icon" color="primary" />
              </Box>
            </Box>
            <Box className="form_fields">
              <InputLabel className="civility_label">Civility</InputLabel>
              <Box className="_input_box">
                <Select
                  onChange={(e) =>
                    setData({ ...data, titlesSelect: e.target.value })
                  }
                  sx={{ m: 1, minWidth: 120 }}
                  size="small"
                  labelId="standard-label"
                  label="Civility"
                  value={data?.titlesSelect ?? ""}
                >
                  <MenuItem value={1}>M.</MenuItem>
                  <MenuItem value={2}>Dr.</MenuItem>
                  <MenuItem value={3}>Ms.</MenuItem>
                  <MenuItem value={4}>Prof.</MenuItem>
                </Select>

                <Input
                  className="_input name_input"
                  required
                  placeholder="Name *"
                  sx={{ m: 1, minWidth: 200 }}
                  value={data?.name ?? ""}
                  onChange={(e) =>
                    setData((data) => ({ ...data, name: e.target.value }))
                  }
                />
                <Input
                  className="_input name_input"
                  required
                  placeholder="First Name *"
                  value={data?.firstName ?? ""}
                  onChange={(e) =>
                    setData({ ...data, firstName: e.target.value })
                  }
                  sx={{ m: 1, minWidth: 300 }}
                />
              </Box>
              <div>
                <p className="head_3">Company</p>
              </div>
              <div>
                <hr className="hr_line_down" />
              </div>

              {/* <InputLabel className="civility_label">Main Company</InputLabel> */}
              <div>
                <Selection
                  sx={{ m: 1, maxWidth: 250 }}
                  required
                  size="small"
                  value={data?.mainPartner?.fullName ?? ""}
                  onOpen={handleMainCompany}
                  handleChange={handleMainCompanyChange}
                  placeholder="Main Compnay"
                  array={MainCompany}
                />
              </div>
            </Box>
          </Box>
          <h5 className="head_3_down">Roles</h5>
          <div>
            <hr className="hr_line_down" />
          </div>
          <div className="name_code">
            <Box className="name_box">
              <h6 className="name_code_p">Name</h6>
            </Box>
            <Box className="code_box">
              <h6 className="name_code_p">Code</h6>
            </Box>
          </div>
          <div>
            <hr className="hr_line_down" />
          </div>

          <Box sx={{ width: "100%" }}>
            <TabContext value={value}>
              <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                <TabList onChange={handleTabChange}>
                  <Tab label="Contact Details" value="0" />
                  <Tab label="Activities" value="1" />
                  <Tab label="Partners" value="2" />
                </TabList>
              </Box>

              <TabPanel value="0">
                <Box className="_lower">
                  <div className="lower_fields">
                    {/* <InputLabel className="civility_label">Function</InputLabel> */}
                    <div>
                      <Selection
                        required
                        sx={{ m: 1, minWidth: 300 }}
                        size="small"
                        value={data?.jobTitleFunction?.name ?? ""}
                        onOpen={handleFunction}
                        placeholder="Function"
                        handleChange={handleFunctionChange}
                        array={FunctionData}
                      />
                    </div>
                    <Input
                      className="_input"

                      placeholder="Fax"
                      sx={{ m: 1, minWidth: 200 }}
                    />
                  </div>
                  <div>
                    <Input
                      className="_input"
                      placeholder="Function/Business card"
                      sx={{ m: 1, minWidth: 200 }}
                    />
                    <div>
                      <Input
                        className="_input"
                        required
                        placeholder="PhoneNumber *"
                        inputProps={{ maxLength: 10 }}
                        sx={{ m: 1, minWidth: 200 }}
                        value={data?.fixedPhone ?? ""}
                        onChange={(e) => {
                          isValidPhoneNumber(e.target.value);

                          setData({ ...data, fixedPhone: e.target.value });
                        }}
                      />
                      <p className="error_label">{phoneText}</p>
                    </div>
                  </div>
                </Box>
                <div>
                  <hr className="hr_line_down" />
                </div>
                <div className="address_input">
                  <Input
                    className="_input_address"
                    placeholder="Address  +"
                    fullWidth
                    sx={{ m: 1, minWidth: 200 }}
                  />
                </div>
              </TabPanel>
              <TabPanel value="1">
                <InputLabel className="panel_2">Upcoming Events</InputLabel>
                <br />
                <table id="table_form">
                  <tbody>
                    <tr id="table_row_form">
                      <th>Type</th>
                      <th>Status</th>
                      <th>Start Date</th>
                      <th>Assigned To</th>
                    </tr>
                    <tr id="table_row_form">
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
                <div>
                  <hr className="hr_line_down" />
                </div>
              </TabPanel>
              <TabPanel value="2">
                <InputLabel className="panel_2">Partners</InputLabel>
                <br />
                <table id="table_form">
                  <tbody>
                    <tr id="table_row_form">
                      <th>Refrence</th>
                      <th>Name</th>
                      <th>FixedPhone</th>
                      <th>Email</th>
                    </tr>
                    <tr id="table_row_form">
                      <td>{`P00${data?.id}` || ""} </td>
                      <td>{data?.name || ""}</td>
                      <td>{data?.fixedPhone || ""}</td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </TabPanel>
            </TabContext>
          </Box>

          <div className="form_btn">
            {/* <Button className="btn" variant="contained" onClick={handleSave}>
            Save
          </Button> */}

            {/* <
             */}
          </div>
        </Card>
        <div className="main_right">
          <InputLabel className="right_settings">Settings</InputLabel>
          <hr className="hr_line_down" />
          <br />
          <InputLabel className="refrence">Reference</InputLabel>
          <Input
            className="_input"
            sx={{ m: 1, minWidth: 200 }}
            value={`P00${data?.id}` || ""}
          />

          {/* <InputLabel className="refrence">Language</InputLabel> */}
          <div>
            <Selection
              sx={{ m: 1, minWidth: 300 }}
              required
              size="small"
              value={data?.language?.name ?? ""}
              onOpen={handleLanguage}
              placeholder="Language"
              handleChange={handleLanguageChange}
              array={LangData}
            />
          </div>

          <InputLabel className="right_settings">Assigned to</InputLabel>
          <hr className="hr_line_down" />
          <div className="assigned">
            <div className="sub_assigned">
              {/* <InputLabel className="civility_label">Assigned To</InputLabel> */}
              <div>
                <Selection
                  sx={{ m: 1, minWidth: 170 }}
                  required
                  size="small"
                  value={data?.user?.fullName ?? ""}
                  onOpen={handleAssignedTo}
                  placeholder="Assigned To"
                  handleChange={assignedToChange}
                  array={assigned}
                />
              </div>
            </div>
            <div className="sub_assigned">
              {/* <InputLabel className="civility_label">Team</InputLabel> */}
              <div>
                <Selection
                  sx={{ m: 1, minWidth: 170 }}
                  required
                  size="small"
                  value={data?.team?.name ?? ""}
                  onOpen={fetchTeamsData}
                  placeholder="Team"
                  handleChange={teamsChange}
                  array={teams}
                />
              </div>
            </div>
          </div>

          <InputLabel className="refrence">Companies associated to</InputLabel>
          <div className="xlr_logo">
            <Selection
              sx={{ m: 1, minWidth: 175 }}
              required
              size="small"
              placeholder="Company Name"
              value={data?.company?.name ?? ""}
              onOpen={fetchCompanyData}
              handleChange={companyChange}
              array={company}
            />
          </div>
          <InputLabel className="right_settings">Notes</InputLabel>
          <div className="editor">
            {/* <ReactDraft /> */}
            {/* <Test/> */}
            <MyEditor />
          </div>
        </div>
      </div>
    </>
  );
}
export default NewForm;
