import React from "react";
import { useState } from "react";
import { Select, MenuItem, Autocomplete, TextField } from "@mui/material";

export default function Selection({
  sx,
  size,
  value,
  onOpen,
  handleChange,
  placeholder,
  array,
}) {
  return (
    <>
      {/* <Select
        sx={sx}
        size={size}
        value={value}
        onOpen={() => onOpen}
        onChange={handleChange}
      >
        {array.map((element) => (
          <MenuItem key={element.id} value={element.id}>
            {element?.name ?? element?.fullName}
          </MenuItem>
        ))}
      </Select> */}
      <Autocomplete
        disablePortal
        id="combo-box-demo"
        value={value ? value : null}
        onChange={(event, value) => {
          handleChange(value);
        }}
       
        options={array.map((element) => element?.fullName ?? element?.name ) }
        sx={sx}
        renderInput={(params) => <TextField {...params} required label={placeholder} />}
      />
    </>
  );
}
