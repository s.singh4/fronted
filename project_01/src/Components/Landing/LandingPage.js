import React from "react";
import { userContext } from "../MyContext";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { Grid } from "@mui/material";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import CardView from "../CardView/CardView";
import "./Landing.css";

const LIMIT = 10;
function LandingPage() {
  const {
    handleDelete,
    cardData,
    loading,
    success1,
    message1,
    open1,
    handleSnackbarClose1,
    setAsset
  } = useContext(userContext);

  const navigate = useNavigate();

  function handleClicks(id) {
    navigate(`/form/${id}`);
   
  }

  return loading ? (
    <div>
      <h2>Loading</h2>
    </div>
  ) : (
    <>
      {success1 ? (
        <Snackbar
          className="snackBar"
          open={open1}
          message={message1}
          onClose={handleSnackbarClose1}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          autoHideDuration={3000}
        >
          <Alert severity="success">{message1}</Alert>
        </Snackbar>
      ) : (
        <Snackbar
          open={open1}
          className="snackBar"
          message={message1}
          onClose={handleSnackbarClose1}
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          autoHideDuration={3000}
        >
          <Alert severity="warning">{message1}</Alert>
        </Snackbar>
      )}
      <div className="grid_view">
        {cardData.length ? (
          cardData?.map((card, key) => {
            return (
              <CardView
                {...card}
                key={key}
                handleClicks={() => handleClicks(card.id)}
                id={card.id}
                card={card}
                handleDelete={() => handleDelete(card.id, cardData)}
              />
            );
          })
        ) : (
          <div>
            <h2>There is no records with this Name</h2>
          </div>
        )}
      </div>
    </>
  );
}

export default LandingPage;
