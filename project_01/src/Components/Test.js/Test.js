import React, { useState } from 'react';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css'; // Import the Quill styles
import "./Test.css"




const MyEditor = () => {
  const [editorContent, setEditorContent] = useState('');

  const handleEditorChange = (content) => {
    setEditorContent(content);
  };

  return (
    <div>
      
      <ReactQuill className='reactquil'
        value={editorContent}
        onChange={handleEditorChange}
        style={{ height: '300px'}}
      />
    </div>
  );
};

export default MyEditor;