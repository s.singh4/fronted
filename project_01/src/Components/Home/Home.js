import React from "react";

import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import "./Home.css";
import TableRow from "@mui/material/TableRow";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";

import { userContext } from "../MyContext";
import { useContext } from "react";

import { useNavigate } from "react-router-dom";

function Home() {
  const {
    handleDelete,
    cardData,
    page,
    loading,
    success1,
    message1,
    open1,
    handleSnackbarClose1,
  } = useContext(userContext);
  const navigate = useNavigate();

  console.log({ cardData });
  const columns = [
    { id: "id", label: "ID", minWidth: 150 },
    { id: "name", label: "Name", minWidth: 150 },
    { id: "fullName", label: "FullName", minWidth: 150 },
    { id: "fixedPhone", label: "PhoneNumber", minWidth: 150 },
    { id: "createdOn", label: "Created On", minWidth: 150 },
  ];

  function handleUpdate(id) {
    const user = id;

    navigate(`/Form/${user}`);
  }

  return loading ? (
    <div>
      <h3>Loading...</h3>
    </div>
  ) : (
    <div>
      {success1 ? (
        <Snackbar
          className="snackBar"
          open={open1}
          message={message1}
          onClose={handleSnackbarClose1}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          autoHideDuration={3000}
        >
          <Alert severity="success">{message1}</Alert>
        </Snackbar>
      ) : (
        <Snackbar
          open={open1}
          className="snackBar"
          message={message1}
          onClose={handleSnackbarClose1}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          autoHideDuration={3000}
        >
          <Alert severity="warning">{message1}</Alert>
        </Snackbar>
      )}
      <Paper md={{ width: "100%", overflow: "hidden" }}>
        <TableContainer md={{ Height: 900 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
                <TableCell> </TableCell>
                <TableCell> </TableCell>
              </TableRow>
            </TableHead>
            <TableBody className="tablebody">
              {cardData.map((row, key) => {
                return (
                  <TableRow className="row" key={row.id}>
                    {columns.map((column, key) => {
                      const value = row[column.id];

                      return (
                        <TableCell key={column.id} align={column.align}>
                          {value}
                        </TableCell>
                      );
                    })}
                    <TableCell>
                      <DeleteIcon
                        id="deleteicon"
                        onClick={() => handleDelete(row.id, cardData)}
                      />
                    </TableCell>
                    <TableCell>
                      <EditIcon
                        id="editicon"
                        onClick={() => handleUpdate(row.id)}
                      />
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
}

export default Home;
