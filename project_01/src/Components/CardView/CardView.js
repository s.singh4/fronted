import React from "react";
import { Card } from "@mui/material";
import PhoneIcon from "@mui/icons-material/Phone";
import WorkIcon from "@mui/icons-material/Work";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
import LanguageIcon from "@mui/icons-material/Language";

import BadgeIcon from "@mui/icons-material/Badge";

import "./CardView.css";
import { CardContent } from "@mui/material";
import { Typography } from "@mui/material";

import Paper from "@mui/material/Paper";

function CardView({
  fixedPhone,
  fullName,
  name,
  jobTitleFunction,
  language,
  handleClicks,
  id,
  card,
  handleDelete,
}) {
  return (
    <>
      <Card
        className="card"
        elevation={8}
        // sx={{ width: "100%", maxWidth: "550px",minWidth:"500px" }}
      >
        <CardContent id="left-content">
          <Paper
            className="papercard"
            variant="outlined"
            square
            // sx={{ m: 1, width: "100%" }}
          >
            {card.picture?.id ? (
              <img
                id="image_input"
                crossOrigin=""
                src={
                  id
                    ? `axelor-erp/ws/rest/com.axelor.meta.db.MetaFile/${card.picture.id}/content/download?image=true&v=${card.picture.version}&parentId=${id}&parentModel=com.axelor.meta.db.Meta`
                    : ""
                }
                height="100%"
                width="100%"
                alt="ProfilePic"
              />
            ) : (
              <img src="" height="100%" width="100%" alt="ProfilePic" />
            )}
          </Paper>

          <Typography className="typo" id="id_name">
            {fullName.toUpperCase() ?? "Not Available"}
          </Typography>
        </CardContent>
        <div className="vl"></div>
        <CardContent id="right-content">
          <div className="top-right">
            <EditIcon id="card_edit" onClick={handleClicks} />
            <DeleteIcon id="delete_edit" onClick={handleDelete} />
          </div>
          <div className="typo">
            <div className="card_subitem">
              <PhoneIcon className="_icons_" />{" "}
              <div> :- {fixedPhone ?? "Not Available"} </div>
            </div>

            <div className="card_subitem">
              <BadgeIcon className="_icons_" />
              <div className="para_div">:-{name ?? "Not Available"}</div>
            </div>
            <div className="card_subitem">
              <LanguageIcon className="_icons_" />
              <div>:-{language?.name ?? "Not Available"}</div>
            </div>

            <div className="card_subitem">
              <WorkIcon className="_icons_" />
              <div>:-{jobTitleFunction?.name ?? " Not Available"}</div>
            </div>
          </div>
        </CardContent>
      </Card>
    </>
  );
}

export default CardView;
