import React from "react";
import { convertToRaw, EditorState } from "draft-js";
import { useState } from "react";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import "./ReactDraft.css";

function ReactDraft() {
  const [editorState, setEditorState] = useState(EditorState.createEmpty());

  const onEditorStateChange = function (editorState) {
    // console.log("-----", editorState.toolbar);
    // console.log("too", "toolbarClassName");
    const selection = editorState.getSelection();
    const currentStyle = editorState.getCurrentInlineStyle();
    console.log("seeee", selection);
    console.log("current", currentStyle);
    setEditorState(editorState);
  };
  console.log("-----", editorState.value);
  return (
    <>
      <Editor
        editorState={editorState}
        className="editor"
        toolbarClassName="toolbar"
        wrapperClassName="wrapper"
        editorClassName="editor"
        onEditorStateChange={onEditorStateChange}
        toolbar={{
          inline: { inDropdown: false,color:"#263d3f" },
        }}
        // onChange={() =>
        //   console.log("hello", editorState.getCurrentInlineStyle())
        // }
      />
    </>
  );
}

export default ReactDraft;
