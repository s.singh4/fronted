import { userContext } from "../MyContext";
import { useContext, useEffect } from "react";
import "./SearchBar.css";
import { debounce } from "lodash";
import SearchOutlinedIcon from "@mui/icons-material/SearchOutlined";

import { InputBase } from "@mui/material";

function SearchBar() {
  const { setPage, setQuery, query, page,oldpage,setOldPage} = useContext(userContext);
  

  const updatequery = (e) => {
    if (e?.target?.value) {
      setOldPage(page)
      setPage(0);
      setQuery(e?.target?.value);
    } else if (e?.target?.value === "") {
      setPage(oldpage)
      setQuery(e?.target?.value);
    }
  };

  const debouncedonChange = debounce(updatequery, 1000);
  console.log("backq", query);

  return (
    <>
      <div className="search">
        <SearchOutlinedIcon className="search_icon" />
        <InputBase
          className="input_"
          defaultValue={query}
          onChange={debouncedonChange}
          placeholder="Search…"
          inputProps={{ "aria-label": "search" }}
        />
      </div>
    </>
  );
}

export default SearchBar;
