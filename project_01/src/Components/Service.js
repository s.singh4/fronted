import React from "react";
import axios from "axios";


let lastCookieString;
let lastCookies = {};

function readCookie(name) {
    let cookieString = document.cookie || "";
    if (cookieString !== lastCookieString) {
        lastCookieString = cookieString;
        lastCookies = cookieString.split("; ").reduce((obj, value) => {
            let parts = value.split("=");
            obj[parts[0]] = parts[1];
            return obj;
        }, {});
    }
    return lastCookies[name];
}
console.log("coookies",document.cookie.split()?.[0]?.split("=")[2])

export const client = axios.create({
  headers: {
    // Authorization: "Basic YWRtaW46YWRtaW4=",
    // "Content-Type": "application/json",
    // "X-CSRF-Token": document.cookie.split()?.[0]?.split("=")[1],
    "X-CSRF-Token": readCookie("CSRF-TOKEN"),
    "X-Requested-With": "XMLHttpRequest",
  },
});
