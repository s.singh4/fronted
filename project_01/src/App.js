import React, { useState, useEffect } from "react";
import { Toolbar } from "@mui/material";
import { AppBar } from "@mui/material";
import { userContext } from "./Components/MyContext";

import { useContext } from "react";
import Pagination from "@mui/material/Pagination";
import Stack from "@mui/material/Stack";
import Typography from "@mui/material/Typography";
import { Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import ListIcon from "@mui/icons-material/List";
import AppsIcon from "@mui/icons-material/Apps";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import ArrowBackIosIcon from "@mui/icons-material/ArrowBackIos";
import SaveIcon from "@mui/icons-material/Save";
import UndoIcon from "@mui/icons-material/Undo";
import { Route, Routes, Link } from "react-router-dom";
import { useLocation } from "react-router-dom";
// import { useHistory } from 'react-router-dom';
import { useNavigate, useParams } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { client } from "./Components/Service";

import { debounce } from "lodash";

import NewForm from "./Components/NewForm/NewForm";

import Home from "./Components/Home/Home";
import LandingPage from "./Components/Landing/LandingPage";
import SearchBar from "./Components/SearchBar/SearchBar";

import "./App.css";
const LIMIT = 10;
const theme = createTheme({
  palette: {
    primary: {
      main: "#808080",
    },
  },
});

function App() {
  const location = useLocation();
  const navigate = useNavigate();
  // const history=useHistory();
  const { id } = useParams();
  const [showCard, setShowcard] = useState(false);

  const { page, setPage, total, data, setData, setQuery, handleSave, goto } =
    useContext(userContext);
  // const [pages, setPages] = useState(1);
  const handleChange = (event, value) => {
    setPage(value);
  };

  const [lastPage, setLastPage] = useState(0);
  useEffect(() => {
    setLastPage(Math.ceil(total / LIMIT) || 1);
  }, [total]);

  function nextPage() {
    if (page < lastPage - 1) {
      setPage((page) => page + 1);
    }
  }
  function prevPage() {
    if (page >= 1) {
      setPage((page) => page - 1);
    }
  }

  const handleShow = debounce(() => {
    if (showCard) {
      nav("/");
    } else {
      nav("/landingpage");
    }
    setShowcard((showCard) => !showCard);
  }, 500);

  const nav = useNavigate();
  async function saveAs() {
    // console.log("staa",handleSave());
    const res=handleSave();
    if(res===0){
      setTimeout(function () {
      navigate(`/`);
    }, 3000);

    }
    
    

    // setTimeout(function () {
    //   navigate(`/`);
    // }, 3000);
  }
  console.log("daaa", data);

  return (
    <div className="App">
      <div className="appbar">
        <AppBar className="app_bar">
          <Toolbar className="tool_bar">
            <div className="left_bar">
              <div className="plusicon" variant="h6">
                <Link to="/form">
                  <AddIcon
                    onClick={() => {
                     console.log("-----",data) 
                      // setData(null);
                      console.log("-----1",data) 
                    }}
                  />
                </Link>
              </div>
              {(location.pathname === "/" ||
                location.pathname === "/landingpage") && (
                <div className="bar_container">
                  <SearchBar />
                </div>
              )}
              {location.pathname !== "/" &&
                location.pathname !== "/landingpage" && (
                  <div className="sb_icons">
                    <div className="undo">
                      <UndoIcon
                        className="undoIcon"
                        onClick={() => {
                          navigate(-1);
                        }}
                      />
                    </div>
                    
                  </div>
                )}
            </div>
            <div className="right_bar">
              <div>
                {(location.pathname === "/landingpage" ||
                  location.pathname === "/") && (
                  <div className="paginate">
                    {/* <Stack spacing={2}>
                      <Pagination
                        className="nums"
                        count={Math.ceil(total / LIMIT)}
                        page={page}
                        siblingCount={0}
                        boundaryCount={0}
                        onChange={handleChange}
                        
                      />
                    </Stack> */}

                    {/* <Button
                      className={
                        page + 1 <= 1
                          ? "disabled_button"
                          : "pagination_btn_arrow"
                      }
                      variant="contained"
                      onClick={prevPage}
                      disabled={page + 1 <= 1}
                    > */}
                    <ArrowBackIosIcon
                      className={
                        page + 1 <= 1
                          ? "disabled_button"
                          : "pagination_btn_arrow"
                      }
                      variant="contained"
                      onClick={prevPage}
                      disabled={page + 1 <= 1}
                    />
                    {/* </Button> */}
                    {/* <ThemeProvider theme={theme}>
                      <Pagination
                        count={Math.ceil(total / LIMIT)}
                        className="pagination_val"
                        color="primary"
                        page={page + 1}
                        hidePrevButton
                        hideNextButton
                      />
                    </ThemeProvider> */}
                    <div className="page_font">{page + 1}</div>

                    {/* <Button
                      className={
                        lastPage - 1 === page
                          ? "disabled_button"
                          : "pagination_btn_arrow"
                      }
                      variant="contained"
                      onClick={nextPage}
                      disabled={lastPage === page}
                    > */}
                    <ArrowForwardIosIcon
                      className={
                        lastPage - 1 === page
                          ? "disabled_button"
                          : "pagination_btn_arrow"
                      }
                      variant="contained"
                      onClick={nextPage}
                      disabled={lastPage === page}
                    />
                    {/* </Button> */}
                  </div>
                )}
              </div>

              <div variant="h6">
                <Button
                  className="btn"
                  variant="contained"
                  onClick={handleShow}
                >
                  {showCard ? <AppsIcon /> : <ListIcon />}
                </Button>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>

      <Routes>
        <Route exact path="/" element={<LandingPage />} />
        <Route exact path="/form" element={<NewForm />} />
        <Route exact path="/form/:id" element={<NewForm />} />
        <Route path="/landingpage" element={<Home />} />
      </Routes>
    </div>
  );
}

export default App;
