import React from "react";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { AuthContext } from "../Auth";
import { AppBar, Toolbar, Typography } from "@mui/material";
import "./NavBar.css";


function NavBar() {
  
  const{user}=useContext(AuthContext);
  return (
    <div>
      <AppBar>
        <Toolbar>
          <ul>
            <li>
              <Typography variant="h7" component="div">
                <Link to="/">Home</Link>
              </Typography>
            </li>
            <li>
              <Typography variant="h7" component="div">
                <Link to="/About">About</Link>
              </Typography>
            </li>
            <li>
              <Typography variant="h7" component="div">
                <Link to="/Profile">Profile</Link>
              </Typography>
            </li>
            <li>
              <Typography variant="h7" component="div">
                {!user && <Link to="/Login">Login</Link>}
              </Typography>
            </li>
          </ul>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default NavBar;
