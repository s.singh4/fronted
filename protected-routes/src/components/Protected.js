import React from "react";
import { useContext } from "react";
import { AuthContext } from "./Auth";
import { Navigate } from "react-router-dom";

export function Protected({ children }) {
  const { user } = useContext(AuthContext);

  if (!user) {
    return <Navigate to="/Login" />;
  }
  return children;
}
