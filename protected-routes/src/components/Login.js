import React from "react";
import { useState, useContext } from "react";
import { AuthContext } from "./Auth";
import { useNavigate } from "react-router-dom";
import "./Login.css";
import { TextField, Button, Grid, Paper } from "@mui/material";

export function Login() {
  const [userDetails, setUserDetails] = useState({
    username: "",
    password: "",
  });
  const { user, setUser } = useContext(AuthContext);

  const navigate = useNavigate();

  const handleChange = () => {
    const admin = user;
    if (
      userDetails.username === userDetails.password &&
      userDetails.password === "admin"
    ) {
      if (!admin) {
        setUser(userDetails.username);
        localStorage.setItem("user", JSON.stringify(userDetails.username));
        navigate("/");
      }
    }
    setUserDetails({ username: "", password: "" });
  };

  return (
    <Grid>
      <Paper elevation={10} className="paper">
        <Grid align="center">
          <h2>Sign In</h2>

          <TextField
            id="outlined-basic"
            label="UserName"
            placeholder="Enter UserName"
            variant="outlined"
            fullWidth
            required
            type="text"
            value={userDetails.username}
            onChange={(e) =>
              setUserDetails({ ...userDetails, username: e.target.value })
            }
          />

          <br></br>
          <br></br>

          <TextField
            id="outlined-basic"
            label="Password"
            variant="outlined"
            type="password"
            placeholder="Enter your Password"
            fullWidth
            required
            value={userDetails.password}
            onChange={(e) =>
              setUserDetails({ ...userDetails, password: e.target.value })
            }
          />

          <br></br>
          <br></br>
          <Button
            size="large"
            variant="contained"
            color="primary"
            fullWidth
            required
            onClick={handleChange}
          >
            Login
          </Button>
        </Grid>
      </Paper>
    </Grid>
  );
}
