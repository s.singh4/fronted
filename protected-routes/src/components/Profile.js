import React from "react";
import { useContext } from "react";
import { AuthContext } from "./Auth";
import { useNavigate } from "react-router-dom";
import Button from "@mui/material/Button";
import { Grid } from "@mui/material";
import "./Home.css";

function Profile() {
  const { user, setUser } = useContext(AuthContext);
  const navigate = useNavigate();
  function handleLogOut() {
    setUser(null);
    localStorage.setItem("user", JSON.stringify(null));
    navigate("/Login");
  }

  return (
    <div className="profile">
      <h1>Welcome {user}</h1>
      <p className="para">
        "On the other hand, we denounce with righteous indignation and dislike
        men who are so beguiled and demoralized by the charms of pleasure of the
        moment, so blinded by desire, that they cannot foresee the pain and
        trouble that are bound to ensue; and equal blame belongs to those who
        fail in their duty through weakness of will, which is the same as saying
        through shrinking from toil and pain. These cases are perfectly simple
        and easy to distinguish
      </p>
      <br></br>
      <br></br>
      <Grid align="center">
        <Button
          size="large"
          variant="contained"
          color="secondary"
          onClick={handleLogOut}
        >
          Logout
        </Button>
      </Grid>
    </div>
  );
}

export default Profile;
