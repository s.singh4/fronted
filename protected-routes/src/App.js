import React from "react";

import NavBar from "./components/NavBar/NavBar";
import "./App.css";
import Home from "./components/Home";
import Profile from "./components/Profile";
import About from "./components/About";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { AuthProvider } from "./components/Auth";
import { Login } from "./components/Login";
import { Protected } from "./components/Protected";

function App() {
  return (
    <AuthProvider>
      <div className="App">
        <BrowserRouter>
          <NavBar />
          <div className="container">
            <Routes>
              <Route path="/" element={<Home />} />
              <Route
                path="/Profile"
                element={
                  <Protected>
                    <Profile />
                  </Protected>
                }
              />
              <Route path="/About" element={<About />} />
              <Route path="/Login" element={<Login />} />
            </Routes>
          </div>
        </BrowserRouter>
      </div>
    </AuthProvider>
  );
}

export default App;
